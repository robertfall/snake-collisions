function moveSnake({ coordinates, direction }) {
  const { x, y } = coordinates;
  switch (direction) {
    case "N":
      return { direction, coordinates: { x, y: y + 1 } };
    case "E":
      return { direction, coordinates: { x: x + 1, y } };
    case "S":
      return { direction, coordinates: { x, y: y - 1 } };
    case "W":
      return { direction, coordinates: { x: x - 1, y } };
  }
}

function turn(direction) {
  switch (direction) {
    case "N":
      return "E";
    case "E":
      return "S";
    case "S":
      return "W";
    case "W":
      return "N";
  }
}


function coordToKey({ x, y }) {
  return `${x},${y}`;
}

const times = (number, callback, seed) => {
  if (number == 1) {
    return callback(seed);
  }
  return callback(times(number - 1, callback, seed));
};

function tick(world, index) {
  if (world.collided) {
    return world;
  }

  const newSnake = moveSnake(world.snake);

  if (world.visited[coordToKey(newSnake.coordinates)]) {
    return Object.assign({}, world, {
      snake: newSnake,
      collided: true
    });
  }

  return {
    snake: newSnake,
    visited: Object.assign({}, world.visited, {
      [coordToKey(newSnake.coordinates)]: true
    })
  };
}

function applyInstruction(world, numberOfSteps, index) {
  if (world.collided) { return world; }
  
  const worldAfterSteps = times(numberOfSteps, tick, world);
  const collidedOn = worldAfterSteps.collided ? index : undefined;

  return Object.assign({}, worldAfterSteps, {
    collidedOn,
    snake: Object.assign({}, worldAfterSteps.snake, {
      direction: turn(worldAfterSteps.snake.direction)
    })
  });
}

function applyInstructions(world, instructions) {
  return ;
}


const snake = {
  coordinates: { x: 0, y: 0 },
  direction: "N"
};

const world = {
  snake,
  visited: { '0,0': true}
};

const instructions = [1,3,3,4,5,6,7];
//5, 6, 8, 4, 2, 1, 8, 9 = 7
//1,1,1,1 = 4
//1,3,3,4,5,6,7 = -1

const finalState = instructions.reduce(applyInstruction, world);

if (finalState.collided) {
  console.log(`${instructions} will collide on ${finalState.collidedOn + 1}`);
} else {
  console.log(`${instructions} will never collide`);
}

const test = {};
